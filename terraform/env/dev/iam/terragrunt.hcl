terraform {
  source = "${get_parent_terragrunt_dir()}//modules/iam"

}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF


provider "aws" {
  region = "${local.c_v.region}"
}
EOF
}

include {
  path = find_in_parent_folders()
}

locals {
  c_v = yamldecode(file(find_in_parent_folders("common_vars.yaml")))
}

inputs = {
  iam_role_name         = "${local.c_v.prefix}-${local.c_v.customer}-${local.c_v.environment}-ci-role"
  iam_group_name        = "${local.c_v.prefix}-${local.c_v.customer}-${local.c_v.environment}-ci-group"
  iam_group_policy_name = "${local.c_v.prefix}-${local.c_v.customer}-${local.c_v.environment}-ci-group-policy"
  iam_user              = "${local.c_v.prefix}-${local.c_v.customer}-${local.c_v.environment}-ci-user"
  pgp_key               = local.c_v.pgp_key
  resource_tags         = local.c_v.resource_tags

}
