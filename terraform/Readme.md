# Terraform IAM module

#### features:
*   The Terraform code is wrapped in Terragrunt to make it more maintainable
*   Terragrunt helps to keep the code dry. https://terragrunt.gruntwork.io/docs/#features
* An [entry template](./common_vars.yaml) to easily manage the configuration inputs.
* Auto generate the docs using terraform-docs. https://github.com/terraform-docs/terraform-docs
* pre-commit hooks to format the code.
* [Bootstrap script](./bootstrap.sh) to setup the development environment

#### pre-requisites:
* The Terraform CLI (1.2.3) installed.
* The Terragrunt CLI (v0.38.1) installed
* The AWS CLI installed.
* The keybase 6.0.2+
* AWS account and associated credentials that allow you to create resources.

#### run:

* run the bootstrap script or setup the pre-req manually.
* setup up a keybase key by using ``` keybase pgp gen```
* update the [entry template](./common_vars.yaml) with pgp key
```sh
---
customer: "phoenix"
environment: "dev"
prefix: "pb"
region: "eu-west-1"
pgp_key: "keybase:<yourusername>"
resource_tags:
  project: phoenix
  environment: dev
```

* export the aws credentials
```sh
export AWS_ACCESS_KEY_ID=<aws_key>
export AWS_SECRET_ACCESS_KEY=<aws_secret>
```

* run terragrunt from the terrafrom/ directory
```sh
terragrunt run-all plan
terragrunt run-all apply
terragrunt run-all destroy
```

* retrieve the aws key and secret from the output
```sh
aws_key = "<key>"
aws_secret ="<secret>"
```

* base64 decode and decrypt the aws_secret
```sh
# run this from the terrafrom/env/dev/iam
terragrunt output -raw aws_secret | base64 --decode  | keybase pgp decrypt
```

###### ref:
* https://registry.terraform.io/providers/hashicorp/aws/latest/docs
* https://terragrunt.gruntwork.io/docs/#reference