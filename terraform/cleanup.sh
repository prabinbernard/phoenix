#!/bin/bash

# Needless to say as all clean up script goes; please test this first on an environement where you have nothing to loose :)
# I use this to tidy up the workspace a bit.
function cleanUp () {

    echo "[INFO]: Starting to clean up terraform and terragrunt temporary files."
    find . -name ".terragrunt-cache" -exec rm -rf {} \;
    find . -name ".terraform" -exec rm -rf {} \;
    find . -name ".terraform.lock.hcl" -exec rm -rf {} \;
    find . -name "terraform.tfstate" -exec rm -rf {} \;
    find . -name "terraform.tfstate.backup" -exec rm -rf {} \;
    echo "[INFO]: Finished cleaning  up."

}

cleanUp
