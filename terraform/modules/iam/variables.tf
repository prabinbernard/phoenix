variable "iam_role_name" {
  description = "The iam role name"
  type        = string
}

variable "path" {
  description = "The path where the resource gets created"
  type        = string
  default     = "/"
}

variable "resource_tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
}

variable "iam_group_name" {
  description = "The iam group name"
  type        = string
}

variable "iam_group_policy_name" {
  description = "The iam group_policy name"
  type        = string
}

variable "iam_user" {
  description = "The iam user name"
  type        = string
}

variable "pgp_key" {
  description = "The pgp key"
  type        = string
}
