output "aws_key" {
  value = aws_iam_access_key.ci_key.id
}

output "aws_secret" {
  value = aws_iam_access_key.ci_key.encrypted_secret
}
