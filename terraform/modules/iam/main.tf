terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

# https://aws.amazon.com/premiumsupport/knowledge-center/iam-assume-role-cli/

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }

  }
}

resource "aws_iam_role" "ci_role" {
  name               = var.iam_role_name
  path               = var.path
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
  tags               = var.resource_tags
}

resource "aws_iam_group" "ci_group" {
  name = var.iam_group_name
  path = var.path
}




resource "aws_iam_group_policy" "ci_group_policy" {
  name  = var.iam_group_policy_name
  group = aws_iam_group.ci_group.name

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["sts:AssumeRole"]
        Effect   = "Allow"
        Resource = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${aws_iam_role.ci_role.name}"
      },
    ]
  })
}

resource "aws_iam_user" "ci_user" {
  name = var.iam_user
  path = var.path

  tags = var.resource_tags
}

resource "aws_iam_access_key" "ci_key" {
  user    = aws_iam_user.ci_user.name
  pgp_key = var.pgp_key
}

resource "aws_iam_user_group_membership" "ci_membership" {
  user = aws_iam_user.ci_user.name

  groups = [
    aws_iam_group.ci_group.name
  ]
}
