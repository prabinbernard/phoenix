#!/bin/bash

# This script will install the essentials to run the terraform code.
# Recommended to run on a virtual or vagrant box as this will overwrite your current versions if any.

function installTerraform() {
    echo"[INFO] Install Terraform"
    wget https://releases.hashicorp.com/terraform/1.2.3/terraform_1.2.3_linux_amd64.zip
    sudo mv terraform_1.2.3_linux_amd64.zip /tmp ;
    cd /tmp ;
    sudo unzip /tmp/terraform_1.2.3_linux_amd64.zip;
    sudo mv /tmp/terraform /usr/local/bin;
    sudo rm /tmp/terraform_1.2.3_linux_amd64.zip;
    sudo chmod o+x /usr/local/bin/terraform;
    cd -;
}

function installTerragrunt() {
    echo"[INFO] Install Terragrunt"
    wget https://github.com/gruntwork-io/terragrunt/releases/download/v0.38.1/terragrunt_linux_amd64
    sudo mv terragrunt_linux_amd64 /usr/local/bin/terragrunt ;
    sudo chmod u+x /usr/local/bin/terragrunt;

}

function installAWScli() {
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install --update
    rm awscliv2.zip
    rm -rf aws
}

function installKeyBase() {
    curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb
    sudo apt install ./keybase_amd64.deb
    rm keybase_amd64.deb
}

installTerraform
installTerragrunt
installAWScli
installKeyBase
