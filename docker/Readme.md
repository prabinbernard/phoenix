# Docker file for litecoin

#### pre-requisites:

* install docker https://docs.docker.com/engine/install/ubuntu/
* install anchore-cli https://github.com/anchore/anchore-cli

#### run:

```sh
docker build -t pbernard/litecoin:latest .
```

#### publish:
```sh
docker push pbernard/litecoin:latest
```

#### test:

```sh
anchore-cli evaluate check "pbernard/litecoin:latest"
Image Digest: sha256:a1dacd285eaac8f2aadad47a2299801343acf25ab7ee53f5960f69e26bad6d4d
Full Tag: docker.io/pbernard/litecoin:latest
Status: pass
Last Eval: 2022-06-27T11:46:02Z
```