#!/bin/sh
set -e

if ([ "$1" = "litecoind" ] || [ "$1" = "litecoin-cli" ] || [ "$1" = "litecoin-tx" ]); then

  echo "Creating the litecoin data directory and setting the ownership to user litecoin"
  mkdir -p "$LITECOIN_DATA"
  chown -R litecoin "$LITECOIN_DATA"

  set -- gosu litecoin "$@"
fi

exec "$@"
