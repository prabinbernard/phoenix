# Helm chart for litecoin

#### pre-requisites:
* A running Kubernetes cluster.
* The Kubernetes cluster API endpoint should be reachable from the machine you are running helm.
* Authenticate the cluster using kubectl and verify the  permissions.
* Install Helm (https://helm.sh/docs/intro/install/)
* Install Kubectl CLI (https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

#### deploy:
```sh
helm upgrade --install litecoin .
```
#### verify:
```sh
kubectl get all
NAME             READY   STATUS    RESTARTS   AGE
pod/litecoin-0   1/1     Running   0          78s

NAME                 TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)                                                     AGE
service/kubernetes   ClusterIP   10.0.0.1      <none>        443/TCP                                                     3d16h
service/litecoin     ClusterIP   10.0.86.149   <none>        9332/TCP,9333/TCP,19332/TCP,19333/TCP,19335/TCP,19443/TCP   78s

NAME                        READY   AGE
statefulset.apps/litecoin   1/1     78s