# Text manipulation using Bash and Python


* Search the /etc/passwd file for users with /bin/bash as the shell and get their home directories.
```sh
grep /bin/bash /etc/passwd | cut -d: -f1,6 | tr ":" " " | awk '{print $2}'
/root
/home/pbernard
/home/john
/home/mary
/home/kevin
```

* The python script is enhanced to take in any shell/command value available in passwd file and display their respective directory structure.
* Pass in the shell value as the first argument to the script; no argument defaults to "/bin/bash" shell.

```sh
python3 homeDir.py
/root
/home/pbernard
/home/john
/home/mary
/home/kevin
```
```sh
python3 homeDir.py /bin/false
/var/lib/tpm
/nonexistent
/run/speech-dispatcher
/run/gnome-initial-setup/
/run/hplip
/var/lib/gdm3
/var/run/vboxadd
```
