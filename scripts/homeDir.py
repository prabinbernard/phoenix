import re
import sys
from subprocess import Popen, PIPE, STDOUT

def findUserHomeDir(shell="", command="cat /etc/passwd"):
  usrDir=[]
  process = Popen(command, stdout = PIPE, stderr = STDOUT, shell = True)
  while True:
    line = process.stdout.readline()
    usrReg = re.compile(r'/.+:'+ shell)
    usrStr = usrReg.search(str(line))
    if usrStr:
      usrTmp = usrStr.group().split(':')[0]
      usrDir.append(usrTmp)
    if not line: break
  return usrDir

if __name__ == "__main__":
    if (sys.platform == "linux"):
      # Eg: values for shell var: "/usr/sbin/nologin", "/bin/false"
      # default shell: /bin/bash
      for homeDir in findUserHomeDir(shell=sys.argv[1] if len(sys.argv) > 1 else "/bin/bash"):
        print(homeDir)
    else:
      print("The script is only tested to work on linux platforms; Exiting!!")
      sys.exit(1)