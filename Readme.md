# Tasks

* [Docker-ayes](./docker/Readme.md)
* [k8s FTW](./helm/Readme.md)
* [All the continuouses](.gitlab-ci.yml)
* [Script kiddies](./scripts/Readme.md)
* [Script grown-ups](./scripts/homeDir.py)
* [Terraform lovers unite](./terraform/Readme.md)